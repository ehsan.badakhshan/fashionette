# TV show search engine project

TV show search engine is a signe web application which has been written in PHP7 (using symfony 5) and Vue2.

## Installation

Make sure that symfony is already installed on your devide [symfony installation](https://symfony.com/doc/current/setup.html).

On project's root derictory start symfony server

```bash
 symfony server:start -d
```

install required composer packages

```bash
 composer install
```

install required front-end packages and libraries

```bash
 npm install
```

initiate builder


```bash
 npm run build
```

## Usage

On your browse enter `https://127.0.0.1:8000/`

## License
[MIT](https://choosealicense.com/licenses/mit/)

## Contact
[Ehsan Badakhshan](https://www.linkedin.com/in/ehsanbadakhshan)

Project Link: https://gitlab.com/ehsan.badakhshan/fashionette