<?php

declare(strict_types=1);


namespace App\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Rest\Route("/api")
 */
final class TvShowFinderController extends AbstractController
{

    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var Client
     */
    private $client;


    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @Rest\Get("/show/{tvShowName}", name="app_get_tv_show")
     * @param string $tvShowName
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function findTvShow(string $tvShowName): JsonResponse
    {
        $uri = "http://api.tvmaze.com/search/shows?q=" . $tvShowName;

        $client = new Client();

        $response = $client->request('GET', $uri);
        $responseBody = $response->getBody();

        $contents = json_decode($responseBody->getContents());

        $data = $this->serializer->serialize($contents, JsonEncoder::FORMAT);
        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }
}
