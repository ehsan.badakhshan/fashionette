import Vue from 'vue';
import { sync } from 'vuex-router-sync';
import App from './App.vue';
import store from './store';
import vuetify from './plugins/vuetify';
import router from './store/routes';

sync(store, router);

const vm = new Vue({
    router,
    components: { App },
    template: '<App/>',
    store,
    vuetify,
}).$mount('#app');

export default vm;
