import Vue from 'vue';
import { SET_TV_SHOWS } from './constants';

export default {
    [SET_TV_SHOWS](state, tvShows) {
        Vue.set(state, 'tvShows', tvShows);
    },
};
