import { SET_TV_SHOWS } from './constants';

import { doFindTvShow } from '../api';

export const findTvShow = async ({ commit }, tvShowName) => {
    const { data } = await doFindTvShow(tvShowName);

    commit(SET_TV_SHOWS, data);
};
