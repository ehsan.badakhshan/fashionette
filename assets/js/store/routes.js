import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

/* eslint-disable no-param-reassign */

const router = new VueRouter({
    mode: "history",
    routes: [
        {
            path: '/',
            name: 'Homepage',
            component: () =>
              import(/* webpackChunkName: "homepage" */ "../pages/TvShowSearch")
        },
        {
            path: '/shows/:showName',
            name: 'TvShows',
            props: true,
            component: () =>
              import(/* webpackChunkName: "shows" */ "../components/TvShows")
        },
    ],

    /* eslint-enable no-param-reassign */
});

export default router;
