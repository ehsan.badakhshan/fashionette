import axios from 'axios';

const BASE_URL = '/api';
const HTTPS = axios.create({ baseURL: BASE_URL });

const additionalHeaders = {};

/* eslint-disable no-undef */

if (typeof CSRF_TOKEN !== 'undefined' && CSRF_TOKEN !== null && CSRF_TOKEN !== '') {
    additionalHeaders.csrf = CSRF_TOKEN;
}

if (typeof AUTHENTICATION !== 'undefined' && AUTHENTICATION !== null && AUTHENTICATION !== '') {
    additionalHeaders.Authentication = AUTHENTICATION;
}
/* eslint-enable no-undef */

HTTPS.interceptors.request.use((config) => ({
    ...config,
    headers: {
        ...config.headers,
        ...additionalHeaders,
    },
}), (error) => Promise.reject(error));

export const doFindTvShow = (address) => HTTPS.get(`/show/${address}`);